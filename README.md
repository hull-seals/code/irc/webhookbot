# Errbot HalpyBOT Hooks
A plugin for ERRBOT to help the Seals with case onboarding.

# Description
This plugin is built to receive, process, and shout out incoming cases, paperwork, and other details from the case onboarding and paperwork forms.

# Installation

## Requirements
- A working ERRBOT instance
- Python 3.6+ OR Python 2+

## Usage
For an installation guide, please visit [ERRBOT's documentation](https://errbot.readthedocs.io/en/latest/user_guide/plugin_development/index.html).

## Troubleshooting
For troubleshooting, please visit [ERRBOT's documentation](https://errbot.readthedocs.io/en/latest/user_guide/plugin_development/testing.html).

# Support
This project is in end-of-life support, and we recommend waiting on the internalization of this plugin into HalpyBOT should you need the functionality of this plugin. However, if you do need direct support, please use this repository's Issues board. 

# Roadmap
This project is in end of life support. No new major features are planned.

# Contributing
If you want to contribute to this or any Seal project, please view [the Welcome Board](https://gitlab.com/hull-seals/welcome-to-the-hull-seals-devops-board).

# Authors and Acknowledgements
Most work done by @BrotherLizardo, with minor revisions by @Rixxan. You can view all authors on our [Contributors](https://gitlab.com/hull-seals/welcome-to-the-hull-seals-devops-board/blob/master/CONTRIBUTORS.md) page. 

# License
This project is goverened by the [GNU General Public License v3.0](LICENSE) license.

# Project Status
This project is in END OF LIFE. Only maintenance and security updates are planned until abandonment. 
