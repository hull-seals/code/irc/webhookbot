from os import path

from errbot import BotPlugin, webhook, botcmd


class halpybot_case(BotPlugin):
    """
    HalpyBOT case plugin for the hullseals
    """

    @webhook
    def newcase(self, payload):
        self.log.debug("The incoming casepayload was :" + str(payload))

        #decode platform
        if payload.get('platform') == "1":
            caseplatform = "PC"
        elif payload.get('platform') == "2":
            caseplatform = "Xbox"
        elif payload.get('platform') == "3":
            caseplatform = "PlayStation"
        else:
            caseplatform = "UNKNOWN"

        #is this a code black
        if payload.get('canopy_breached') == "1":
            #decode synth
            if payload.get('can_synth') == "1":
                cansynth = "No"
            else:
                cansynth = "Yes"

            #build message
            mymessage = (
				f"CODEBLACK -~~- {payload['cmdr_name']} -~~- {caseplatform} -~~- {payload['system']} -~~- {payload['hull']} -~~- {cansynth} -~~- {payload['o2_timer']}"
			)
        elif payload.get('platform') == "1":
            #build message
            mymessage = (
				f"PC -~~- {payload['cmdr_name']} -~~- {caseplatform} -~~- {payload['system']} -~~- {payload['hull']}"
			)
        elif payload.get('platform') == "2":
            #build message
            mymessage = (
				f"XB -~~- {payload['cmdr_name']} -~~- {caseplatform} -~~- {payload['system']} -~~- {payload['hull']}"
            )
        elif payload.get('platform') == "3":
            #build message
            mymessage = (
				f"PS4 -~~- {payload['cmdr_name']} -~~- {caseplatform} -~~- {payload['system']} -~~- {payload['hull']}"
            )

        else:
            #build message
            mymessage = (
				f"PLTERR -~~- {payload['cmdr_name']} -~~- {caseplatform} -~~- {payload['system']} -~~- {payload['hull']}"
            )

        #send long message to channels
        arfinchan = ["#case-notify-2"]
        for chan in arfinchan:
            self.send(
              self.build_identifier(chan), mymessage,
            )

        return "OK"

    @webhook
    def fishcase(self, payload):
        self.log.debug("The incoming fishcasepayload was :" + str(payload))

        #decode platform
        if payload.get('platform') == "1":
            caseplatform = "PC"
        elif payload.get('platform') == "2":
            caseplatform = "Xbox"
        elif payload.get('platform') == "3":
            caseplatform = "PlayStation"
        else:
            caseplatform = "UNKNOWN"

        #decode type
        if payload.get('case_type') == "8":
            casetypeshort = "Lift"
        elif payload.get('case_type') == "9":
            casetypeshort = "Golf"
        elif payload.get('case_type') == "10":
            casetypeshort = "Puck"
        elif payload.get('case_type') == "11":
            casetypeshort = "Pick"
        else:
            casetypeshort = "UNKNOWN"

        if payload.get('platform') == "1":
            #build message
            mymessage = (
				f"PCFISH -~~- {payload['cmdr_name']} -~~- {caseplatform} -~~- {payload['system']} -~~- {payload['planet']} -~~- {payload['curr_cord']} -~~- {casetypeshort}"
			)

        elif payload.get('platform') == "2":
            #build message
            mymessage = (
				f"XBFISH -~~- {payload['cmdr_name']} -~~- {caseplatform} -~~- {payload['system']} -~~- {payload['planet']} -~~- {payload['curr_cord']} -~~- {casetypeshort}"
            )

        elif payload.get('platform') == "3":
            #build message
            mymessage = (
				f"PSFISH -~~- {payload['cmdr_name']} -~~- {caseplatform} -~~- {payload['system']} -~~- {payload['planet']} -~~- {payload['curr_cord']} -~~- {casetypeshort}"
            )

        else:
            #build message
            mymessage = (
				f"PLTERRFISH -~~- {payload['cmdr_name']} -~~- {caseplatform} -~~- {payload['system']} -~~- {payload['planet']} -~~- {payload['curr_cord']} -~~- {casetypeshort}"
            )

        #send long message to channels
        arfinchan = ["#case-notify-2"]

        for chan in arfinchan:
            self.send(
              self.build_identifier(chan), mymessage,
            )

        return "OK"

    @webhook
    def ppwk(self, payload):
        self.log.debug("The incoming paperwork was :" + str(payload))

        #build message
        mymessage = (
			f"PPWK -~~- {payload['client']} -~~- {payload['seal']}"
        )

        #send long message to channels
        self.send(
          self.build_identifier("#case-notify-2"), mymessage,
        )

        return "OK"
